## Opis
Remake gry 8-bitowej River Raid na silniku Godot. Pierwszy projekt w 6. edycji Game Dev School (2018/2019).

## Autorzy
* Dariusz Ruchała (programista)
* Mikołaj Ziółkowski (grafik)
* Daniel Woźniak (designer)
* Paweł Trojanowski (desinger)