shader_type canvas_item;

void fragment() {
	vec2 waves_uv = UV;
	waves_uv.y += cos(TIME * 1.5 + (UV.x + UV.y) * 8.0) * 0.035;
	waves_uv.x += sin(TIME * 1.5 + (UV.x + UV.y) * 8.0) * 0.035;
	COLOR = texture(TEXTURE, waves_uv);
}

