extends Polygon2D

onready var timer = $Timer2
onready var audio_player = $AudioStreamPlayer
onready var hud = get_tree().get_root().get_node("Node/HUD")
onready var global = get_node("/root/Global")

func _ready():
	timer.connect("timeout", self, "_on_timeout")
	audio_player.play()
	
func _on_timeout():
	hud.number_of_notifications -= 1
	queue_free()

func set_label(achievement_id):
	var achievement = global.achievements_dictionary[achievement_id]
	$AchievementLabel.set_text(achievement.name)
	var image = load("res://graphics/achievements/" + achievement.icon)
	$Sprite.set_texture(image)