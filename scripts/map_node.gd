extends Node2D

const RESET_COORDINATES_THRESHOLD = -10000000.0

onready var camera = $Camera2D
onready var player = $Player
onready var global = get_node("/root/Global")
onready var level_label = get_tree().get_root().get_node("Node/HUD/Level")

var camera_init_position
var player_init_position

var last_random_stage = null
var stages = ["stage_1.tscn", "stage_2.tscn", "stage_3.tscn", "stage_4.tscn", "stage_5.tscn"]
var new_stage_position_y = 0
var stage_instances = []


func _ready():
	randomize()
	camera_init_position = camera.get_camera_position()
	player_init_position = player.position
	global.set_stats_zeros()
	global.play_menu_soundtrack(false)
	global.reset_achievement_thats_what_im_talking_about()
	global.reset_achievement_old_school()
	global.reset_achievement_are_you_immortal()
	load_stage(get_random_stage())
	
func load_stage(stage_name):
	print("load_stage(%s)" % stage_name)
	var stage_scene = load("res://scenes/stages/%s" % stage_name)
	var stage_instance = stage_scene.instance()
	stage_instance.position.y = new_stage_position_y
	new_stage_position_y -= stage_instance.MAP_HEIGHT
	add_child(stage_instance)
	stage_instances.append(stage_instance)
	
func get_random_stage():
	var rand_idx = randi() % len(stages)
	while stages[rand_idx] == last_random_stage:
		rand_idx = randi() % len(stages)
	last_random_stage = stages[rand_idx]
	return stages[rand_idx]

func _process(delta):
	reset_coordinates()
	load_another_stage()
		
func reset_coordinates():
	var camera_y = camera.get_camera_position().y
	if camera_y < RESET_COORDINATES_THRESHOLD:
		print("Reset coordinates.")
		get_node("Player").position.y -= camera_y
		get_node("Camera2D").position.y -= camera_y
		#get_node("TileMap").position.y -= camera_y
		#get_node("TileMap2").position.y -= camera_y
		
func load_another_stage():
	var camera_y = camera.get_camera_position().y
	if camera_y - 1000 < new_stage_position_y:
		load_stage(get_random_stage())
		
func reset_game_state():
	camera.position = camera_init_position
	camera.position.y -= (level_label.level - 1) * stage_instances[0].MAP_HEIGHT
	player.position = player_init_position
	player.position.y -= (level_label.level - 1) * stage_instances[0].MAP_HEIGHT
	for stage_instance in stage_instances:
		stage_instance.respawn_enemies_and_fuel()