extends Area2D
const TYPE = "bridge"

const SCORE = 600

onready var kill_strike_label = get_tree().get_root().get_node("Node/HUD/KillStrike")
onready var score_label = get_tree().get_root().get_node("Node/HUD/Score")
onready var camera = get_tree().get_root().get_node("Node/Map/Camera2D")
onready var collision_shape = $CollisionShape2D
onready var global = get_node("/root/Global")
var explosion_scene = preload("res://scenes/bridge_explosion.tscn")
var is_destroyed = false
var points_scene = preload("res://scenes/points.tscn")

func _ready():
	connect("area_entered", self, "_on_area_entered")
	pass
	
func _on_area_entered(other):
	if other.TYPE == "bullet" and not is_destroyed:
		is_destroyed = true
		hide()
		var explosion_instance = explosion_scene.instance()
		explosion_instance.position = position
		get_parent().add_child(explosion_instance)
		other.destroy()
		global.number_of_accurate_shots += 1
		kill_strike_label.increment()
		score_label.add(SCORE * kill_strike_label.point_multiplier)
		
		var points_instance = points_scene.instance()
		get_parent().add_child(points_instance)
		points_instance.position = position
		points_instance.position.y -= 50
		points_instance.set_points(SCORE)
		
		camera.start_shake()
		
func respawn():
	is_destroyed = false
	show()