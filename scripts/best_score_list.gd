extends GridContainer

var leaderboard = null

onready var global = get_node("/root/Global")
onready var confirm_nickname_label = get_parent().get_node("ConfirmNicknameLabel")
onready var new_game_button = get_parent().get_node("NewGameButton")
onready var main_menu_button = get_parent().get_node("MainMenuButton")
onready var summary_button = get_parent().get_node("SummaryButton")

var leaderboard_label = preload("res://scenes/leaderboard/leaderboard_label.tscn")
var leaderboard_label_short = preload("res://scenes/leaderboard/leaderboard_label_short.tscn")
var leaderboard_label_yellow = preload("res://scenes/leaderboard/leaderboard_label_yellow.tscn")
var leaderboard_label_yellow_short = preload("res://scenes/leaderboard/leaderboard_label_yellow_short.tscn")
var leaderboard_field = preload("res://scenes/leaderboard/leaderboard_field.tscn")

var new_position = null
var player_field = null

func _ready():
	global.play_menu_soundtrack(true)
	
	load_from_file()
	
	if global.score != null and global.leaderboard_new_position == null:
		append_new_result(global.score)
		save_to_file()
		
	add_labels()
	
	if new_position != null:
		global.leaderboard_new_position = new_position
	
	if new_position == null or new_position >= 11:
		confirm_nickname_label.hide()
		new_game_button.grab_focus()
	else:
		new_game_button.hide()
		main_menu_button.hide()
		summary_button.hide()

func load_from_file():
	var leaderboard_file = File.new()
	
	if leaderboard_file.file_exists("user://leaderboard.json"):
		leaderboard_file.open("user://leaderboard.json", File.READ)
		leaderboard = parse_json(leaderboard_file.get_as_text())
		leaderboard_file.close()
	else:
		leaderboard = parse_json("{\"data\": []}")
	
func save_to_file():
	var leaderboard_file = File.new()
	leaderboard_file.open("user://leaderboard.json", File.WRITE)
	leaderboard_file.store_string(to_json(leaderboard))
	leaderboard_file.close()
	
func add_labels():
	var i = 1
	for result in leaderboard["data"]:
		print_result(i, result)
		i += 1
	if new_position == 11:
		print_result_outside_top(global.score)
		
func print_result(i, result):
	if global.leaderboard_new_position == i:
		var position = leaderboard_label_yellow_short.instance()
		position.set_text("%d.   " % i)
		add_child(position)
		
		var player = leaderboard_label_yellow.instance()
		player.set_text("%s   " % result["player"])
		add_child(player)
		
		var score = leaderboard_label_yellow_short.instance()
		score.set_text("%d" % result["score"])
		add_child(score)
		
	elif new_position != i:
		var position = leaderboard_label_short.instance()
		position.set_text("%d.   " % i)
		add_child(position)
		
		var player = leaderboard_label.instance()
		player.set_text("%s   " % result["player"])
		add_child(player)
		
		var score = leaderboard_label_short.instance()
		score.set_text("%d" % result["score"])
		add_child(score)
		
	else:
		var position = leaderboard_label_yellow_short.instance()
		position.set_text("%d.   " % i)
		add_child(position)
		
		var player = leaderboard_field.instance()
		add_child(player)
		player_field = player
		player.grab_focus()
		player.connect("text_entered", self, "_on_text_entered")
		
		var score = leaderboard_label_yellow_short.instance()
		score.set_text("%d" % result["score"])
		add_child(score)
		
func print_result_outside_top(p_result):
	if p_result == null:
		return
	
	var position = leaderboard_label_yellow_short.instance()
	position.set_text("      ")
	add_child(position)
	
	var player = leaderboard_label_yellow_short.instance()
	player.set_text("Your result")
	add_child(player)
	
	var score = leaderboard_label_yellow_short.instance()
	score.set_text("%d" % p_result)
	add_child(score)
	
func append_new_result(score):
	var idx = leaderboard["data"].size() - 1
	while idx >= 0 and leaderboard["data"][idx]["score"] < score:
		idx -= 1
		
	idx += 1
	new_position = idx + 1
	leaderboard["data"].insert(idx, {"player": "Unknown", "score": score})
	
	if leaderboard["data"].size() == 11:
		leaderboard["data"].pop_back()
	
func _on_text_entered(text):
	leaderboard["data"][new_position - 1]["player"] = text
	save_to_file()
	
	hide_confirm_and_show_buttons()
	
	var player_label = leaderboard_label_yellow.instance()
	player_label.set_text(text)
	player_field.replace_by(player_label)
	
func hide_confirm_and_show_buttons():
	confirm_nickname_label.hide()
	new_game_button.show()
	main_menu_button.show()
	summary_button.show()
	new_game_button.grab_focus()