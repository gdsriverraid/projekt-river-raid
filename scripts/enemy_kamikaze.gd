extends "res://scripts/enemy.gd"

const SCORE = 100

export var init_speed = 200
export var horizontal_speed = 100

var explosion_scene = preload("res://scenes/plane_explosion.tscn")
var smoke_trail_scene = preload("res://scenes/smoke_trail.tscn")
onready var smoke_trails = get_tree().get_root().get_node("Node/Map/SmokeTrails")
var speed = 0 
var init_position = null
onready var player = get_tree().get_root().get_node("Node/Map/Player")
onready var audio_engine = $AudioEngine
onready var tile_map = get_node("../..")
onready var level_label = get_tree().get_root().get_node("Node/HUD/Level")

var smoke_trail_instance = null

func _ready():
	init_position = position
	$VisibilityNotifier2D.connect("screen_entered", self, "_on_screen_entered")
	create_smoke_trail()
	
func create_smoke_trail():
	smoke_trail_instance = smoke_trail_scene.instance()
	smoke_trail_instance.amount = 20
	smoke_trail_instance.lifetime = 1
	smoke_trails.add_child(smoke_trail_instance)
	
func remove_smoke_trail():
	smoke_trail_instance.queue_free()
	smoke_trail_instance = null
	
func _process(delta):
	delta = level_label.modify_delta_time(delta)
	position.y += speed * delta
	if speed != 0:
		var x_replacement = horizontal_speed * delta
		var distance = abs(position.x - player.position.x)
		if distance < abs(x_replacement):
			x_replacement = distance
		
		if position.x < player.position.x:
			position.x += x_replacement
		else:
			position.x -= x_replacement
			
		process_smoke_trail()
			
func process_smoke_trail():
	var new_position = position + tile_map.position
	
	if new_position.y < 1080:
		smoke_trail_instance.position = new_position

func _on_screen_entered():
	speed = init_speed
	position.x = player.position.x
	audio_engine.play()
	
func _on_area_entered(other):
	base_on_area_entered(other, SCORE)
	if other.TYPE == "bullet":
		speed = 0
		create_explosion()
		audio_engine.stop()
		increment_achievement_not_this_time()
		increment_achievement_suicadal()
		smoke_trail_instance.emitting = false
	elif other.TYPE == "player":
		hide()
		collision_poligon.disabled = true
		create_explosion()
		audio_engine.stop()
		smoke_trail_instance.emitting = false
		
func create_explosion():
	var explosion_instance = explosion_scene.instance()
	explosion_instance.position = position
	get_parent().add_child(explosion_instance)
	
func respawn():
	remove_smoke_trail()
	create_smoke_trail()
	position = init_position
	show()
	$CollisionShape2D.disabled = false
	
func increment_achievement_not_this_time():
	var achievement = global.achievements_dictionary["NotThisTime"]
	if not achievement["unlocked"]:
		achievement["progress"] += 1
		if achievement["progress"] == achievement["goal"]:
			achievement["unlocked"] = true
			global.emit_unlocked_achievement("NotThisTime")
		global.save_achievements()

func increment_achievement_suicadal():
	var achievement = global.achievements_dictionary["Suicadal"]
	if not achievement.unlocked:
		achievement.progress += 1
		if achievement.progress >= achievement.goal:
			achievement.unlocked = true
			global.emit_unlocked_achievement("Suicadal")
			global.save_achievements()