extends Control

onready var global = get_node("/root/Global")

func _ready():
	for button in get_tree().get_nodes_in_group("controls_button"):
			button.set_event_scancode(global.settings_dictionary[button.key])
	$SoundSlider.value = global.settings_dictionary["sound_volume"]

func save_settings():
	for button in get_tree().get_nodes_in_group("controls_button"):
		global.settings_dictionary[button.key] = button.event_scancode
	global.settings_dictionary["sound_volume"] = $SoundSlider.value
	
	var settings_file = File.new()
	settings_file.open(global.SETTINGS_FILE_PATH, File.WRITE)
	settings_file.store_string(to_json(global.settings_dictionary))
	settings_file.close()
	
func remove_duplicates(event_scancode):
	if event_scancode == null:
		return
		
	for child in get_tree().get_nodes_in_group("controls_button"):
		if child.event_scancode == event_scancode:
			child.set_event_scancode(null)