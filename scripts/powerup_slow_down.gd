extends Area2D
const TYPE = "powerup_slow_down"

export(int) var duration = 10
export(int) var slow_down_speed = 50

onready var global = get_node("/root/Global")
onready var collision_shape = $CollisionShape2D

func _ready():
	connect("area_entered", self, "_on_area_entered")
	
func _on_area_entered(other):
	if other.TYPE == "player":
		$AudioStreamPlayer.play()
		other.set_powerup_slow_down(duration, slow_down_speed)
		hide()
		collision_shape.disabled = true
		global.collected_powerups += 1
		increment_achievement_slow_mo()
		increment_achievement_thats_what_im_talking_about()
		increment_achievement_old_school()
		
func respawn():
	show()
	collision_shape.disabled = false

func increment_achievement_slow_mo():
	var achievement = global.achievements_dictionary["SlowMo"]
	if not achievement["unlocked"]:
		achievement["progress"] += 1
		if achievement["progress"] == achievement["goal"]:
			achievement["unlocked"] = true
			global.save_achievements()
			global.emit_unlocked_achievement("SlowMo")
		
func increment_achievement_thats_what_im_talking_about():
	var achievement = global.achievements_dictionary["ThatsWhatImTalkingAbout"]
	if not achievement.unlocked and achievement.progress_slowing < achievement.goal_slowing:
		achievement.progress_slowing += 1
		if achievement.progress_lifes >= achievement.goal_lifes and \
				achievement.progress_faster >= achievement.goal_faster and \
				achievement.progress_slowing >= achievement.goal_slowing:
			achievement.unlocked = true
			global.save_achievements()
			global.emit_unlocked_achievement("ThatsWhatImTalkingAbout")
			
func increment_achievement_old_school():
	var achievement = global.achievements_dictionary["OldSchool"]
	if not achievement.unlocked:
		achievement.progress += 1