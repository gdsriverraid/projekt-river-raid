extends Area2D
const TYPE = "fuel"

const SCORE = 30

onready var kill_strike_label = get_tree().get_root().get_node("Node/HUD/KillStrike")
onready var collision_shape = $CollisionShape2D
var explosion_scene = preload("res://scenes/barrel_explosion.tscn")
onready var global = get_node("/root/Global")
var points_scene = preload("res://scenes/points.tscn")

func _ready():
	connect("area_entered", self, "_on_area_entered")
	pass
	
func _on_area_entered(other):
	if other.TYPE == "bullet":
		hide()
		collision_shape.disabled = true
		kill_strike_label.increment()
		get_tree().get_root().get_node("Node/HUD/Score").add(SCORE * kill_strike_label.point_multiplier)
		var explosion_instance = explosion_scene.instance()
		explosion_instance.position = position
		
		var points_instance = points_scene.instance()
		get_parent().add_child(points_instance)
		points_instance.position = position
		points_instance.position.y -= 50
		points_instance.set_points(SCORE)
		
		get_parent().add_child(explosion_instance)
		increment_achievement_heli_above()
		
func respawn():
	show()
	collision_shape.disabled = false
	
func increment_achievement_heli_above():
	var achievement = global.achievements_dictionary["IdRatherFly"]
	if not achievement["unlocked"]:
		achievement["progress"] += 1
		if achievement["progress"] == achievement["goal"]:
			achievement["unlocked"] = true
			global.save_achievements()
			global.emit_unlocked_achievement("IdRatherFly")