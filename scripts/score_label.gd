extends Label

onready var global = get_node("/root/Global")
var sum_score = 0

func add(score):
	sum_score += score
	global.score = sum_score
	achievement_add_score_dont_stop_me(score)
	achievement_add_score_going_for_it(score)
	achievement_add_score_wow_nice_one(score)
	set_text("%.0f" % sum_score)
	
func achievement_add_score_dont_stop_me(score):
	var achievement = global.achievements_dictionary["DontStopMe"]
	if not achievement.unlocked:
		achievement.progress += score
		if achievement.progress >= achievement.goal:
			achievement.unlocked = true
			global.save_achievements()
			global.emit_unlocked_achievement("DontStopMe")
			
func achievement_add_score_going_for_it(score):
	var achievement = global.achievements_dictionary["GoingForIt"]
	if not achievement.unlocked:
		achievement.progress += score
		if achievement.progress >= achievement.goal:
			achievement.unlocked = true
			global.save_achievements()
			global.emit_unlocked_achievement("GoingForIt")
			
func achievement_add_score_wow_nice_one(score):
	var achievement = global.achievements_dictionary["WowNiceOne"]
	if not achievement.unlocked:
		achievement.progress += score
		if achievement.progress >= achievement.goal:
			achievement.unlocked = true
			global.save_achievements()
			global.emit_unlocked_achievement("WowNiceOne")