extends Label

onready var point_multiplier_label = get_tree().get_root().get_node("Node/HUD/PointMultiplier")
onready var global = get_node("/root/Global")

var kill_strike = 0
var point_multiplier = 1

func _ready():
	hide()
	point_multiplier_label.hide()
	pass

func reset():
	kill_strike = 0
	point_multiplier = 1
	hide()
	point_multiplier_label.hide()
	
func increment():
	kill_strike += 1
	if kill_strike > global.longest_kill_strike:
		global.longest_kill_strike = kill_strike
	
	if kill_strike > 50:
		point_multiplier = 3
	elif kill_strike > 40:
		point_multiplier = 2.5
	elif kill_strike > 30:
		point_multiplier = 2
	elif kill_strike > 20:
		point_multiplier = 1.5
	elif kill_strike > 10:
		point_multiplier = 1.3
	
	if kill_strike == 11:
		show()
		point_multiplier_label.show()
		
	if kill_strike > 10:
		set_text("Kill Strike: %d" % kill_strike)
		point_multiplier_label.set_text("Multiplier %.1f" % point_multiplier)