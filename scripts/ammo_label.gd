extends Node2D

export(int) var init_ammo = 10
export(int) var max_ammo = 12
var ammo = init_ammo

var bullet_white = preload("res://graphics/hud/bullet_white.png")
var bullet_gray = preload("res://graphics/hud/bullet_gray.png")
onready var label = $Label

func _ready():
	refresh_textures()

func add_ammo(amount):
	ammo += amount
	if ammo > max_ammo:
		ammo = max_ammo
	refresh_textures()
	label.set_text("%d/%d" % [ammo, max_ammo])
	if ammo > 0:
		label.add_color_override("font_color", Color(1,1,1))
	
func decrement_ammo():
	ammo -= 1
	if ammo < 0:
		ammo = 0
	refresh_textures()
	label.set_text("%d/%d" % [ammo, max_ammo])
	if ammo == 0:
		label.add_color_override("font_color", Color(0.5,0.5,0.5))

func reset_ammo():
	ammo = init_ammo
	refresh_textures()
	label.set_text("%d/%d" % [ammo, max_ammo])
	label.add_color_override("font_color", Color(1,1,1))

func is_full():
	return ammo == max_ammo
	
func refresh_textures():
	for ammo in get_children():
		if ammo is Sprite:
			ammo.texture = bullet_gray
	
	if ammo >= 1:
		$Ammo1.texture = bullet_white
		if ammo >= 2:
			$Ammo2.texture = bullet_white
			if ammo >= 3:
				$Ammo3.texture = bullet_white
				if ammo >= 4:
					$Ammo4.texture = bullet_white
					if ammo >= 5:
						$Ammo5.texture = bullet_white
						if ammo >= 6:
							$Ammo6.texture = bullet_white
							if ammo >= 7:
								$Ammo7.texture = bullet_white
								if ammo >= 8:
									$Ammo8.texture = bullet_white
									if ammo >= 9:
										$Ammo9.texture = bullet_white
										if ammo >= 10:
											$Ammo10.texture = bullet_white
											if ammo >= 11:
												$Ammo11.texture = bullet_white
												if ammo >= 12:
													$Ammo12.texture = bullet_white
					