extends Area2D
const TYPE = "player"

var bullet_scene = preload("res://scenes/bullet.tscn")
var explosion_scene = preload("res://scenes/plane_explosion.tscn")
var water_drops_scene = preload("res://scenes/water_particles.tscn")
var ready_to_shoot = true
onready var timer = $Timer
onready var game_over_timer = $GameOverTimer

onready var powerups_label = get_tree().get_root().get_node("Node/HUD/PowerupsLabel")
onready var fuel_gauge = get_tree().get_root().get_node("Node/HUD/FuelGauge")
onready var level_label = get_tree().get_root().get_node("Node/HUD/Level")
onready var lives_label = get_tree().get_root().get_node("Node/HUD/Lives")
onready var ammo_label = get_tree().get_root().get_node("Node/HUD/Ammo")
onready var score_label = get_tree().get_root().get_node("Node/HUD/Score")
onready var kill_strike_label = get_tree().get_root().get_node("Node/HUD/KillStrike")
onready var camera = get_tree().get_root().get_node("Node/Map/Camera2D")
onready var player_bullets = get_tree().get_root().get_node("Node/Map/PlayerBullets")
onready var smoke_trail = get_tree().get_root().get_node("Node/Map/SmokeTrails/PlayerSmokeTrail")
onready var audio_game_over = $AudioGameOver
onready var global = get_node("/root/Global")

onready var map = get_parent()
onready var collision_shape = $CollisionShape2D
onready var sprite = $Sprite
onready var audio_shot = $AudioShot
onready var audio_engine = $AudioEngine
onready var audio_fueling = $AudioFueling
onready var audio_respawn = $AudioRespawn
onready var powerup_slow_down_timer = $PowerupSlowDownTimer
onready var powerup_faster_shooting_timer = $PowerupFasterShootingTimer
onready var engine_flame = $EngineFlame

export(int) var init_speed = 300
export(int) var new_level_speed_increase = 0

var level_speed = init_speed
var speed = init_speed
var is_fueling = false
var is_destroyed = false

var powerup_slow_down_speed = 0

# controls
onready var key_left = global.settings_dictionary["player_1_left"]
onready var key_right = global.settings_dictionary["player_1_right"]
onready var key_up = global.settings_dictionary["player_1_up"]
onready var key_down = global.settings_dictionary["player_1_down"]
onready var key_fire = global.settings_dictionary["player_1_fire"]

enum EngineState {
	SLOW_DOWN, NORMAL, SPEED_UP
}

var player_audio_bus_idx = AudioServer.get_bus_index("PlayerEngine")

func set_engine_state(engine_state):
	match engine_state:
		EngineState.NORMAL:
			AudioServer.set_bus_effect_enabled(player_audio_bus_idx, 0, false)
			AudioServer.set_bus_effect_enabled(player_audio_bus_idx, 1, false)
			engine_flame.play("Normal")
		EngineState.SLOW_DOWN:
			AudioServer.set_bus_effect_enabled(player_audio_bus_idx, 0, false)
			AudioServer.set_bus_effect_enabled(player_audio_bus_idx, 1, true)
			engine_flame.play("SlowDown")
		EngineState.SPEED_UP:
			AudioServer.set_bus_effect_enabled(player_audio_bus_idx, 0, true)
			AudioServer.set_bus_effect_enabled(player_audio_bus_idx, 1, false)
			engine_flame.play("SpeedUp")

func _ready():
	set_engine_state(EngineState.NORMAL)
	connect("area_entered", self, "_on_area_entered")
	connect("area_exited", self, "_on_area_exited")
	connect("body_entered", self, "_on_body_entered")
	timer.connect("timeout", self, "_on_timeout_complete")
	powerup_slow_down_timer.connect("timeout", self, "_on_powerup_slow_down_timeout")
	powerup_faster_shooting_timer.connect("timeout", self, "_on_powerup_faster_shooting_timeout")
	game_over_timer.connect("timeout", self, "_on_game_over_timeout")
	audio_engine.play()
	reset_achievement_bulls_eye()

func _on_area_entered(other):
	match other.TYPE:
		"enemy": 
			print("game_over(), enemy=%s" % other.name)
			game_over(false)
		"enemy_bullet":
			print("game_over(), enemy=%s" % other.name)
			game_over(false)
		"bridge":
			if other.is_destroyed:
				level_label.next_level()
				level_speed += new_level_speed_increase
				speed = level_speed
				score_label.add(lives_label.lives * 300)
				check_achievement_bulls_eye()
				reset_achievement_bulls_eye()
			else:
				print("game_over(), enemy=%s" % other.name)
				game_over(false)
		"fuel":
			fueling(true, other)
		
func _on_area_exited(other):
	if other.TYPE == "fuel":
		fueling(false, other)
	
func _on_body_entered(other):
	print("Detected ground collision. Restarting.")
	print("game_over(), enemy=%s" % other.name)
	game_over(false)

func _process(delta):
	delta = level_label.modify_delta_time(delta)
	var x_replacement = 0;
	var y_replacement = -1 * speed * delta
	
	if Input.is_key_pressed(key_left):
		x_replacement = -600 * delta;
	if Input.is_key_pressed(key_right):
		x_replacement = 600 * delta;
	if Input.is_key_pressed(key_up):
		y_replacement += y_replacement * 0.5
	if Input.is_key_pressed(key_down):
		y_replacement -= y_replacement * 0.5
	if Input.is_key_pressed(key_fire):
		player_shoot()
	
	position.x += x_replacement
	position.y += y_replacement
	global.distance += abs(y_replacement)
	limit_player_position()
	
	# increase amount of fuel if fueling
	if is_fueling:
		fuel_gauge.increase(50 * delta)
	# decrease amount of fuel
	if y_replacement != 0:
		fuel_gauge.decrease(abs(y_replacement) / 58)
		
	if not is_destroyed and fuel_gauge.fuel_amount == 0:
		print("game_over(), enemy=fuel")
		game_over(true)
		
	smoke_trail.position = position
	smoke_trail.position.y += 44

func _input(event):
	if not is_destroyed and event is InputEventKey:
		if event.scancode == key_up:
			if event.pressed:
				set_engine_state(EngineState.SPEED_UP)
			else:
				set_engine_state(EngineState.NORMAL)
		elif event.scancode == key_down:
			if event.pressed:
				set_engine_state(EngineState.SLOW_DOWN)
			else:
				set_engine_state(EngineState.NORMAL)
		
func limit_player_position():
	var PLAYER_EXTEND_Y = 32
	var camera_top = camera.position.y - (camera.get_viewport_rect().size.y / 2)
	var camera_bottom = camera.position.y + (camera.get_viewport_rect().size.y / 2) - 120
	var player_top = position.y - PLAYER_EXTEND_Y
	var player_bottom = position.y + PLAYER_EXTEND_Y
	
	if player_top < camera_top:
		position.y = camera_top + PLAYER_EXTEND_Y
	elif player_bottom > camera_bottom:
		position.y = camera_bottom - PLAYER_EXTEND_Y
		
func game_over(is_fuel_empty):
	audio_engine.stop()
	audio_fueling.stop()
	is_destroyed = true
	collision_shape.disabled = true
	sprite.hide()
	engine_flame.hide()
	speed = 0
	if is_fuel_empty:
		var water_drops_instance = water_drops_scene.instance()
		water_drops_instance.position = position
		get_parent().add_child(water_drops_instance)
	else:
		var explosion_instance = explosion_scene.instance()
		explosion_instance.position = position
		get_parent().add_child(explosion_instance)
		
	if lives_label.lives == 0:
		audio_game_over.play()
		game_over_timer.wait_time = 2
	else:	
		game_over_timer.wait_time = 1
	game_over_timer.start()
	smoke_trail.emitting = false
	
func _on_game_over_timeout():
	if lives_label.lives > 0:
		lives_label.decrease()
		restart_from_checkpoint()
	else:
		get_tree().change_scene("res://scenes/game_over.tscn");
		
func restart_from_checkpoint():
	set_engine_state(EngineState.NORMAL)
	reset_achievement_bulls_eye()
	audio_respawn.play()
	smoke_trail.emitting = true
	smoke_trail.restart()
	fuel_gauge.reset()
	map.reset_game_state()
	speed = level_speed
	collision_shape.disabled = false
	sprite.show()
	engine_flame.show()
	is_destroyed = false
	audio_engine.play()
	powerup_slow_down_timer.stop()
	powerup_slow_down_speed = 0
	timer.wait_time = 0.5
	ammo_label.reset_ammo()
	kill_strike_label.reset()
	global.reset_achievement_old_school()
	global.reset_achievement_are_you_immortal()
	powerups_label.reset()

func player_shoot():
	if not is_destroyed and ready_to_shoot and ammo_label.ammo > 0:
		ready_to_shoot = false
		timer.start()
		var x = position.x
		var y = position.y
		print("shoot (x,y)=(%s,%s)" % [x, y])
		ammo_label.decrement_ammo()
		
		var bullet_instance = bullet_scene.instance()
		bullet_instance.position.x = x
		bullet_instance.position.y = y - 50
		player_bullets.add_child(bullet_instance)
		audio_shot.play()
		
	
func _on_timeout_complete():
	ready_to_shoot = true
	
func fueling(on_entered, fuel_barrel):
	if on_entered:
		print("fueling on_entered")
		is_fueling = true
		speed = (level_speed - powerup_slow_down_speed) / 2
		audio_fueling.play()
		fuel_gauge.last_barrel = fuel_barrel
	else:
		print("fueling on_exited")
		is_fueling = false
		speed = (level_speed - powerup_slow_down_speed)
		audio_fueling.stop()
		
func set_powerup_slow_down(duration, slow_down_speed):
	print("powerup_slow_down begin")
	if powerup_slow_down_speed == 0:
		powerup_slow_down_speed = slow_down_speed
		speed = level_speed - slow_down_speed
		
	powerup_slow_down_timer.wait_time = duration
	powerup_slow_down_timer.start()
	powerups_label.play_slow_down()
	
func _on_powerup_slow_down_timeout():
	speed = level_speed
	powerup_slow_down_speed = 0
	if is_fueling:
		speed /= 2
	print("powerup_slow_down end")
	
func set_faster_shooting(duration):
	print("powerup_faster_shooting_timer begin")
	timer.wait_time = 0.17
	powerup_faster_shooting_timer.wait_time = duration
	powerup_faster_shooting_timer.start()
	powerups_label.play_faster_shooting()
	
func _on_powerup_faster_shooting_timeout():
	timer.wait_time = 0.5
	
# achievements

func check_achievement_bulls_eye():
	var achievement = global.achievements_dictionary["BullsEye"]
	if not achievement.unlocked and achievement.progress == 0:
		achievement.unlocked = true
		global.save_achievements()
		global.emit_unlocked_achievement("BullsEye")
		
func reset_achievement_bulls_eye():
	var achievement = global.achievements_dictionary["BullsEye"]
	if not achievement.unlocked:
		achievement.progress = 0