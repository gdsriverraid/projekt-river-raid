extends CanvasLayer

onready var camera = get_node("../Map/Camera2D")
onready var camera_coordinates = get_node("DebugCameraCoordinates")

onready var player = get_node("../Map/Player")
onready var player_coordinates = get_node("DebugPlayerCoordinates")
onready var debug_speed_label = get_node("DebugSpeed")

var achievement_notification_scene = preload("res://scenes/achievement_notification.tscn")
var number_of_notifications = 0

onready var global = get_node("/root/Global")

func _ready():
	global.connect("unlocked_achievement", self, "_on_unlocked_achievement")

func _process(delta):
	var camera_position = camera.get_camera_position()
	camera_position = [camera_position.x, camera_position.y]
	camera_coordinates.set_text("camera (x,y): (%s,%s)" % camera_position)
	
	var player_position = [player.position.x, player.position.y]
	player_coordinates.set_text("player (x,y): (%s,%s)" % player_position)

	debug_speed_label.set_text("camera_speed: %d" % player.speed)
	
func _on_unlocked_achievement(achievement_id):
	print("_on_unlocked_achievement")
	var achievement_notification_instance = achievement_notification_scene.instance()
	add_child(achievement_notification_instance)
	achievement_notification_instance.set_label(achievement_id)
	achievement_notification_instance.position.y = 84 * number_of_notifications
	number_of_notifications += 1