extends Container

func set_name(name):
	$Name.set_text(name)
	
func set_description(description):
	$Description.set_text(description)
	
func set_progress(progress):
	if progress == null:
		$Progress.set_text("LOCKED")
	else:
		$Progress.set_text("LOCKED | Progress: %d" % progress)
	
func set_difficulty(difficulty):
	$Difficulty.set_text(difficulty)
	
func set_icon(icon_file):
	var image = load("res://graphics/achievements/" + icon_file)
	$Sprite.texture = image

func set_unlocked(is_unlocked):
	if is_unlocked:
		$Progress.set_text("UNLOCKED")
		$Sprite.material = null
