extends Node2D

var lives = 3

onready var global = get_node("/root/Global")

onready var life_1 = $Life1
onready var life_2 = $Life2
onready var life_3 = $Life3
onready var life_x = $LifeX
onready var how_many_label = $HowMany
onready var zero = $Zero

func _ready():
	refresh_images()

func decrease():
	if lives > 0:
		lives -= 1
		refresh_images()
		
func increase():
	lives += 1
	increment_achievement_thats_what_im_talking_about()
	refresh_images()
	
func reset():
	lives = 3
	refresh_images()

func increment_achievement_thats_what_im_talking_about():
	var achievement = global.achievements_dictionary["ThatsWhatImTalkingAbout"]
	if not achievement.unlocked and achievement.progress_lifes < achievement.goal_lifes:
		achievement.progress_lifes += 1
		if achievement.progress_lifes >= achievement.goal_lifes and \
				achievement.progress_faster >= achievement.goal_faster and \
				achievement.progress_slowing >= achievement.goal_slowing:
			achievement.unlocked = true
			global.save_achievements()
			global.emit_unlocked_achievement("ThatsWhatImTalkingAbout")
			
func refresh_images():
	match lives:
		0:
			zero.show()
			life_1.hide()
			life_2.hide()
			life_3.hide()
			life_x.hide()
			how_many_label.hide()
		1:
			zero.hide()
			life_1.hide()
			life_2.show()
			life_3.hide()
			life_x.hide()
			how_many_label.hide()
		2:
			zero.hide()
			life_1.show()
			life_2.show()
			life_3.hide()
			life_x.hide()
			how_many_label.hide()
		3:
			zero.hide()
			life_1.show()
			life_2.show()
			life_3.show()
			life_x.hide()
			how_many_label.hide()
		_:
			zero.hide()
			life_1.hide()
			life_2.hide()
			life_3.hide()
			life_x.show()
			how_many_label.show()
			how_many_label.set_text("%dx" % lives)
