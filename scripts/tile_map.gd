extends Node2D

const MAP_HEIGHT = 18176

onready var camera = get_node("../Camera2D");
onready var map_node = get_tree().get_root().get_node("Node/Map")

func _ready():
	pass

func _process(delta):
	var camera_bottom = camera.position.y + get_viewport_rect().size.y
	var tile_map_top = position.y - MAP_HEIGHT
	
	if camera_bottom < tile_map_top:
		print("map: queue_free()")
		map_node.stage_instances.remove(0)
		queue_free()
		#if abs(camera_bottom - tile_map_top) > 1000:
			#print("Moving tile map warning! camera_bottom=%s, tile_map_top=%s" % [camera_bottom, tile_map_top])
		#else:
			#print("Moved tile map.")
			#position.y -= 2 * MAP_HEIGHT
			#respawn_enemies_and_fuel()
			

func respawn_enemies_and_fuel():
	for object in $Objects.get_children():
		if object.has_method("respawn"):
			object.respawn()