extends PopupMenu

onready var global = get_node("/root/Global")
onready var resume_button = get_node("VBoxContainer/Buttons/ResumeButton")
onready var restart_button = get_node("VBoxContainer/Buttons/RestartButton")
onready var exit_button = get_node("VBoxContainer/Buttons/ExitButton")

var paused_sounds = []

func _ready():
	resume_button.connect("pressed", self, "_on_resume_pressed")
	restart_button.connect("pressed", self, "_on_restart_pressed")
	exit_button.connect("pressed", self, "_on_exit_pressed")

func _input(event):
	if event is InputEventKey and \
			not event.echo and event.pressed and event.scancode == KEY_ESCAPE:
		if visible:
			_on_resume_pressed()
		else:
			global.play_button_sound()
			get_tree().paused = true
			show()
			resume_button.grab_focus()
			play_game_sounds(false)

func _on_resume_pressed():
	global.play_button_sound()
	hide()
	get_tree().paused = false
	play_game_sounds(true)
	
func _on_restart_pressed():
	global.play_button_sound()
	get_tree().change_scene("res://scenes/map.tscn")
	get_tree().paused = false
	
func _on_exit_pressed():
	global.play_button_sound()
	get_tree().change_scene("res://scenes/main_menu.tscn")
	get_tree().paused = false
	
func play_game_sounds(is_playing):
	if is_playing:
		for paused_sound in paused_sounds:
			paused_sound["sound"].play(paused_sound["playback_position"])
	else:
		var sounds = get_tree().get_nodes_in_group("audio_game")
		paused_sounds = []
		for sound in sounds:
			if sound.playing:
				var paused_sound = {
						"sound": sound,
						"playback_position": sound.get_playback_position()
					}
				sound.stop()
				paused_sounds.append(paused_sound)