extends "res://scripts/enemy.gd"

const SCORE = 60

export(int) var init_speed = 300

onready var timer = $Timer
var speed = 0
var init_position = null
var explosion_scene = preload("res://scenes/helicopter_explosion.tscn")
onready var audio_engine = $AudioEngine
onready var visibility_notifier = $VisibilityNotifier2D

var is_on_screen = false
onready var level_label = get_tree().get_root().get_node("Node/HUD/Level")

func _ready():
	randomize()
	init_position = position
	random_direction()
	get_node("Propeller/AnimationPlayer").play("propeller")

	visibility_notifier.connect("screen_entered", self, "_on_screen_entered")
	visibility_notifier.connect("screen_exited", self, "_on_screen_exited")
	connect("body_entered", self, "_on_body_entered")
	timer.connect("timeout", self, "_on_timeout")

func _on_area_entered(other):
	base_on_area_entered(other, SCORE)
	if other.TYPE == "bullet":
		var explosion_instance = explosion_scene.instance()
		explosion_instance.position = position
		get_parent().add_child(explosion_instance)
		audio_engine.stop()
		increment_achievement_heli_above()
	
func _on_body_entered(other):
	speed = -speed
	init_speed = -init_speed
	if speed >= 0:
		rotation = 0
	else:
		rotation = PI
	
func _on_screen_entered():
	timer.start()
	is_on_screen = true
	
func _on_screen_exited():
	is_on_screen = false
	
func _on_timeout():
	print("helicopter start to move")
	speed = init_speed
	audio_engine.play()
	
func _process(delta):
	delta = level_label.modify_delta_time(delta)
	position.x -= speed * delta
	
func respawn():
	speed = 0
	timer.stop()
	random_direction()
	position = init_position
	show()
	collision_poligon.disabled = false
	audio_engine.stop()
	if is_on_screen:
		timer.start()
	
func random_direction():
	var direction = randi() % 2
	if direction == 1:
		init_speed = -1 * abs(init_speed)
		rotation = PI
	else:
		init_speed = abs(init_speed)
		rotation = 0
		
func increment_achievement_heli_above():
	var achievement = global.achievements_dictionary["HeliAbove"]
	if not achievement["unlocked"]:
		achievement["progress"] += 1
		if achievement["progress"] == achievement["goal"]:
			achievement["unlocked"] = true
			global.emit_unlocked_achievement("HeliAbove")
			global.save_achievements()
		