extends Area2D
const TYPE = "enemy_bullet"

export(int) var speed = 250

onready var level_label = get_tree().get_root().get_node("Node/HUD/Level")

func _ready():
	yield($VisibilityNotifier2D, "screen_exited")
	print("enemy_bullet: queue_free()")
	queue_free()
	pass

func _process(delta):
	delta = level_label.modify_delta_time(delta)
	position.y += speed * delta
