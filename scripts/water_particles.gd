extends Node2D

onready var timer = $Timer
onready var particles = $Particles2D
onready var audio_player = $AudioStreamPlayer2D

func _ready():
	timer.connect("timeout", self, "_on_timeout")
	timer.start()
	particles.restart()
	audio_player.play()

func _on_timeout():
	print("water_particles queue_free()")
	queue_free()
