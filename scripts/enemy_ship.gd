extends "res://scripts/enemy.gd"

const SCORE = 30

export(int) var init_speed = 100

onready var timer = $Timer
onready var visibility_notifier = $VisibilityNotifier2D
var explosion_scene = preload("res://scenes/ship_explosion.tscn")
onready var audio_engine = $AudioEngine
onready var level_label = get_tree().get_root().get_node("Node/HUD/Level")

var init_position = null
var speed = 0
var is_on_screen = false

func _ready():
	randomize()
	init_position = position
	random_direction()
	visibility_notifier.connect("screen_entered", self, "_on_screen_entered")
	visibility_notifier.connect("screen_exited", self, "_on_screen_exited")
	connect("body_entered", self, "_on_body_entered")
	timer.connect("timeout", self, "_on_timeout")

func _on_area_entered(other):
	base_on_area_entered(other, SCORE)
	if other.TYPE == "bullet":
		var explosion_instance = explosion_scene.instance()
		explosion_instance.position = position
		get_parent().add_child(explosion_instance)
		audio_engine.stop()
		timer.stop()
	
func _on_body_entered(other):
	speed = -speed
	init_speed = -init_speed
	$Sprite.flip_v = not $Sprite.flip_v
	
func _on_screen_entered():
	print("ship _on_screen_entered()")
	timer.start()
	is_on_screen = true
	
func _on_screen_exited():
	print("ship _on_screen_exited()")
	is_on_screen = false
	
func _on_timeout():
	speed = init_speed
	audio_engine.play()
	
func _process(delta):
	delta = level_label.modify_delta_time(delta)
	position.x -= speed * delta
	
func respawn():
	show()
	random_direction()
	collision_poligon.disabled = false
	position = init_position
	speed = 0
	timer.stop()
	audio_engine.stop()
	if is_on_screen:
		timer.start()
	
func random_direction():
	var direction = randi() % 2
	if direction == 1:
		init_speed = -1 * abs(init_speed)
		$Sprite.flip_v = true
	else:
		init_speed = abs(init_speed)
		$Sprite.flip_v = false
	