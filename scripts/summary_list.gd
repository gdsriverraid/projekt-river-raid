extends GridContainer

onready var global = get_node("/root/Global")

func _ready():
	global.play_menu_soundtrack(true)
	
	$DefeatedEnemiesValue.set_text(String(global.defeated_enemies))
	$DistanceValue.set_text("%.0f km" % (global.distance / 5000.0))
	$FuelConsumedValue.set_text("%.0f L" % global.fuel_consumed)
	$CollectedPowerupsValue.set_text(String(global.collected_powerups))
	$NumberOfShotsValue.set_text(String(global.number_of_shots))
	
	if global.number_of_shots > 0:
		var accuracy = (global.number_of_accurate_shots / float(global.number_of_shots)) * 100
		$AccuracyValue.set_text("%.0f %%" % accuracy)
	else:
		$AccuracyValue.set_text("--")
	$LongestKillStrikeValue.set_text(String(global.longest_kill_strike))
	
