extends Node2D

onready var progress_bar_faster_shooting = get_node("FasterShooting/ProgressBar")
onready var progress_bar_slow_down = get_node("SlowDown/ProgressBar")
onready var animation_player_faster_shooting = get_node("FasterShooting/ProgressBar/AnimationPlayer")
onready var animation_player_slow_down = get_node("SlowDown/ProgressBar/AnimationPlayer")
onready var sprite_faster_shooting = get_node("FasterShooting/Sprite")
onready var sprite_slow_down = get_node("SlowDown/Sprite")

var material_grayscale = preload("res://materials/achievement_material.tres")

func _ready():
	animation_player_faster_shooting.connect("animation_finished", self, "_on_faster_shooting_animation_finished")
	animation_player_slow_down.connect("animation_finished", self, "_on_slow_down_animation_finished")
	reset()
	
func reset():
	sprite_faster_shooting.material = material_grayscale
	sprite_slow_down.material = material_grayscale
	animation_player_faster_shooting.stop()
	animation_player_slow_down.stop()
	progress_bar_faster_shooting.set_value(0)
	progress_bar_slow_down.set_value(0)
	
func play_faster_shooting():
	sprite_faster_shooting.material = null
	animation_player_faster_shooting.play("Animation")
	
func play_slow_down():
	sprite_slow_down.material = null
	animation_player_slow_down.play("Animation")
	
func _on_slow_down_animation_finished(anim_name):
	sprite_slow_down.material = material_grayscale
	
func _on_faster_shooting_animation_finished(anim_name):
	sprite_faster_shooting.material = material_grayscale