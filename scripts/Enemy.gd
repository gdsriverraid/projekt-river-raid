extends Area2D
const TYPE = "enemy"

var powerup_ammo_scene = preload("res://scenes/ammo.tscn")
onready var collision_poligon = $CollisionShape2D
onready var kill_strike_label = get_tree().get_root().get_node("Node/HUD/KillStrike")
onready var global = get_node("/root/Global")
var points_scene = preload("res://scenes/points.tscn")

func _ready():
	connect("area_entered", self, "_on_area_entered")
	pass
	
func _process(delta):
	pass
	
func base_on_area_entered(other, score):
	if other.TYPE == "bullet":
		hide()
		global.defeated_enemies += 1
		collision_poligon.disabled = true
		kill_strike_label.increment()
		get_tree().get_root().get_node("Node/HUD/Score").add(score * kill_strike_label.point_multiplier)
		var powerup_ammo_instance = powerup_ammo_scene.instance()
		powerup_ammo_instance.is_temporary = true
		powerup_ammo_instance.position = position
		get_parent().add_child(powerup_ammo_instance)
		
		var points_instance = points_scene.instance()
		get_parent().add_child(points_instance)
		points_instance.position = position
		points_instance.position.y -= 50
		points_instance.set_points(score)
		
		increment_achievement_gimme_more()
		increment_achievement_that_would_be_it()
	
func respawn():
	show()
	collision_poligon.disabled = false
	
func getScore():
	pass
	
func increment_achievement_gimme_more():
	var achievement = global.achievements_dictionary["GimmeMore"]
	if not achievement.unlocked:
		achievement.progress += 1
		if achievement.progress >= achievement.goal:
			achievement.unlocked = true
			global.emit_unlocked_achievement("GimmeMore")
		global.save_achievements()
			
func increment_achievement_that_would_be_it():
	var achievement = global.achievements_dictionary["ThatWouldBeIt"]
	if not achievement.unlocked:
		achievement.progress += 1
		if achievement.progress >= achievement.goal:
			achievement.unlocked = true
			global.emit_unlocked_achievement("ThatWouldBeIt")
		global.save_achievements()