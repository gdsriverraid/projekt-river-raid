extends Area2D
const TYPE = "bullet"

onready var player = get_tree().get_root().get_node("Node/Map/Player")
onready var animation = get_node("Sprite/AnimationPlayer")
onready var global = get_node("/root/Global")

var bullet_explosion_scene = preload("res://scenes/bullet_explosion.tscn")

export(int) var bullet_speed = 500
var final_speed = null

onready var level_label = get_tree().get_root().get_node("Node/HUD/Level")

func _ready():
	increment_achievement_bulls_eye()
	global.number_of_shots += 1
	final_speed = player.speed + bullet_speed
	animation.play("shot")
	connect("area_entered", self, "_on_area_entered")
	connect("body_entered", self, "_on_body_entered")
	yield(get_node("VisibilityNotifier2D"), "screen_exited")
	print("bullet: queue_free()")
	queue_free()

func _process(delta):
	delta = level_label.modify_delta_time(delta)
	position.y -= final_speed * delta

func _on_area_entered(other):
	# let to go through object
	if other.TYPE == "enemy" or other.TYPE == "fuel" or other.TYPE == "bridge":
		global.number_of_accurate_shots += 1
		destroy()
		decrement_achievement_bulls_eye()
	
func _on_body_entered(other):
	destroy()
	var bullet_explosion_instance = bullet_explosion_scene.instance()
	bullet_explosion_instance.position = position
	get_parent().add_child(bullet_explosion_instance)
	
func destroy():
	hide()
	queue_free()
	
func increment_achievement_bulls_eye():
	var achievement = global.achievements_dictionary["BullsEye"]
	if not achievement.unlocked:
		achievement.progress += 1
		
func decrement_achievement_bulls_eye():
	var achievement = global.achievements_dictionary["BullsEye"]
	if not achievement.unlocked:
		achievement.progress -= 1
	