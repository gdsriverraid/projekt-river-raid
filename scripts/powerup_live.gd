extends Area2D
const TYPE = "powerup_faster_shooting"

onready var global = get_node("/root/Global")
onready var lives_label = get_tree().get_root().get_node("Node/HUD/Lives")
onready var collision_shape = $CollisionShape2D

func _ready():
	connect("area_entered", self, "_on_area_entered")
	
func _on_area_entered(other):
	if other.TYPE == "player":
		$AudioStreamPlayer.play()
		lives_label.increase()
		hide()
		collision_shape.disabled = true
		global.collected_powerups += 1
		increment_achievement_souvenirs()
		increment_achievement_old_school()
	
func increment_achievement_souvenirs():
	var achievement = global.achievements_dictionary["Souvenirs"]
	if not achievement["unlocked"]:
		achievement["progress"] += 1
		if achievement["progress"] >= achievement["goal"]:
			achievement["unlocked"] = true
			global.save_achievements()
			global.emit_unlocked_achievement("Souvenirs")
		
func increment_achievement_old_school():
	var achievement = global.achievements_dictionary["OldSchool"]
	if not achievement.unlocked:
		achievement.progress += 1