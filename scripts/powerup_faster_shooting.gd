extends Area2D
const TYPE = "powerup_faster_shooting"

export(int) var duration = 5

onready var global = get_node("/root/Global")
onready var collision_shape = $CollisionShape2D

func _ready():
	connect("area_entered", self, "_on_area_entered")
	
func _on_area_entered(other):
	if other.TYPE == "player":
		$AudioStreamPlayer.play()
		other.set_faster_shooting(duration)
		hide()
		collision_shape.disabled = true
		global.collected_powerups += 1
		increment_achievement_you_got_the_moves()
		increment_achievement_thats_what_im_talking_about()
		increment_achievement_old_school()
		
func respawn():
	show()
	collision_shape.disabled = false
	
func increment_achievement_you_got_the_moves():
	var achievement = global.achievements_dictionary["YouGotTheMoves"]
	if not achievement["unlocked"]:
		achievement["progress"] += 1
		if achievement["progress"] == achievement["goal"]:
			achievement["unlocked"] = true
			global.save_achievements()
			global.emit_unlocked_achievement("YouGotTheMoves")
		
func increment_achievement_thats_what_im_talking_about():
	var achievement = global.achievements_dictionary["ThatsWhatImTalkingAbout"]
	if not achievement.unlocked and achievement.progress_faster < achievement.goal_faster:
		achievement.progress_faster += 1
		if achievement.progress_lifes >= achievement.goal_lifes and \
				achievement.progress_faster >= achievement.goal_faster and \
				achievement.progress_slowing >= achievement.goal_slowing:
			achievement.unlocked = true
			global.save_achievements()
			global.emit_unlocked_achievement("ThatsWhatImTalkingAbout")
			
func increment_achievement_old_school():
	var achievement = global.achievements_dictionary["OldSchool"]
	if not achievement.unlocked:
		achievement.progress += 1