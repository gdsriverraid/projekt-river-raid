extends "res://scripts/enemy.gd"

const SCORE = 100

onready var camera = get_tree().get_root().get_node("Node/Map/Camera2D")
onready var smoke_trails = get_tree().get_root().get_node("Node/Map/SmokeTrails")
var explosion_scene = preload("res://scenes/plane_explosion.tscn")
var smoke_trail_scene = preload("res://scenes/smoke_trail.tscn")
onready var audio_engine = $AudioEngine
onready var tile_map = get_node("../..")
onready var level_label = get_tree().get_root().get_node("Node/HUD/Level")

export(int) var init_speed = 600
var speed = 0
var init_position = null
var bellow_screen = false

var smoke_trail_position_x_offset = 0
var smoke_trail_instance = null

func _ready():
	init_position = position
	randomize()
	random_direction()
	create_particles()
	
func create_particles():
	smoke_trail_instance = smoke_trail_scene.instance()
	smoke_trail_instance.amount = 256
	smoke_trail_instance.lifetime = 4
	smoke_trails.add_child(smoke_trail_instance)
	
func remove_particles():
	smoke_trail_instance.queue_free()
	smoke_trail_instance = null
	
func _process(delta):
	delta = level_label.modify_delta_time(delta)
	if not bellow_screen and speed == 0:
		start()
	if not bellow_screen and speed != 0:
		stop()
	position.x += speed * delta
	process_smoke_trail()
	
func process_smoke_trail():
	var new_position = position + tile_map.position
	new_position.x += smoke_trail_position_x_offset
	
	if new_position.x > 0 and new_position.x < 1920:
		smoke_trail_instance.position = new_position

func _on_area_entered(other):
	base_on_area_entered(other, SCORE)
	if other.TYPE == "bullet":
		var explosion_instance = explosion_scene.instance()
		explosion_instance.position = position
		get_parent().add_child(explosion_instance)
		increment_achievement_whos_next()
		smoke_trail_instance.emitting = false
		
func respawn():
	remove_particles()
	create_particles()
	bellow_screen = false
	speed = 0
	position = init_position
	random_direction()
	show()
	collision_poligon.disabled = false
	audio_engine.stop()

func random_direction():
	var direction = randi() % 2
	if direction == 1:
		init_speed = -1 * abs(init_speed)
		#$#Sprite.flip_v = true
		rotation = PI
		position.x = get_viewport_rect().size.x + abs(position.x)
		smoke_trail_position_x_offset = 40
	else:
		init_speed = abs(init_speed)
		#$Sprite.flip_v = false
		rotation = 0
		smoke_trail_position_x_offset = -40
		
func start():
	var camera_top = camera.position.y - (get_viewport_rect().size.y / 2)
	var plane_bottom = tile_map.position.y + position.y + 64
	
	if plane_bottom > camera_top:
		print("enemy plane start")
		speed = init_speed
		audio_engine.play()
		
func stop():
	var camera_bottom = camera.position.y + (get_viewport_rect().size.y / 2)
	var plane_top = tile_map.position.y + position.y - 64
	
	if plane_top > camera_bottom:
		print("enemy plane stop")
		bellow_screen = true
		speed = 0
		audio_engine.stop()
		
func increment_achievement_whos_next():
	var achievement = global.achievements_dictionary["WhosNext"]
	if not achievement["unlocked"]:
		achievement["progress"] += 1
		if achievement["progress"] == achievement["goal"]:
			achievement["unlocked"] = true
			global.emit_unlocked_achievement("WhosNext")
			global.save_achievements()