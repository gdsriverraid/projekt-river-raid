extends Node2D

export var speed = 100
onready var timer = $Timer
onready var label = $Label
onready var kill_strike_label = get_tree().get_root().get_node("Node/HUD/KillStrike")

func _ready():
	timer.connect("timeout", self, "_on_timeout")
	timer.start()
	match kill_strike_label.point_multiplier:
		1:
			label.add_color_override("font_color", Color(1,1,1))
		1.3:
			label.add_color_override("font_color", Color(0,1,0))
		1.5:
			label.add_color_override("font_color", Color(0,0,1))
		2:
			label.add_color_override("font_color", Color(0.64,0.27,0.64))
		2.5:
			label.add_color_override("font_color", Color(0.64,0.27,0.64))
		3:
			label.add_color_override("font_color", Color(0.93,0.84,0))

func _process(delta):
	position.y -= speed * delta

func _on_timeout():
	queue_free()
	
func set_points(points):
	label.set_text("%d" % (points * kill_strike_label.point_multiplier))