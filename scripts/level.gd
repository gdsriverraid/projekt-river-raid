extends Label

export var time_modifier = 0.1

var level = 1
onready var audio_level = $AudioStreamPlayer
onready var global = get_node("/root/Global")

func next_level():
	audio_level.play()
	level += 1
	set_text(str(level))
	unlock_achievement_we_got_a_badass_over_here()
	unlock_achievement_old_school()
	increment_achievement_are_you_immortal()
	
func reset_level():
	level = 1
	set_text(str(level))

func unlock_achievement_we_got_a_badass_over_here():
	var achievement = global.achievements_dictionary["WeGotABadassOverHere"]
	if not achievement.unlocked:
		achievement.unlocked = true
		global.save_achievements()
		global.emit_unlocked_achievement("WeGotABadassOverHere")

func unlock_achievement_old_school():
	var achievement = global.achievements_dictionary["OldSchool"]
	if not achievement.unlocked: 
		if achievement.progress == 0:
			achievement.unlocked = true
			global.save_achievements()
			global.emit_unlocked_achievement("OldSchool")
		else:
			achievement.progress = 0
			
func increment_achievement_are_you_immortal():
	var achievement = global.achievements_dictionary["AreYouImmortal"]
	if not achievement.unlocked:
		achievement.progress += 1
		if achievement.progress >= 5:
			achievement.unlocked = true
			global.save_achievements()
			global.emit_unlocked_achievement("AreYouImmortal")
			
func modify_delta_time(delta):
	return (((level - 1) * time_modifier) + 1) * delta