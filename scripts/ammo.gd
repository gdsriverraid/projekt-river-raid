extends Area2D
const TYPE = "powerup_ammo"

onready var ammo_label = get_tree().get_root().get_node("Node/HUD/Ammo")
onready var collision_shape = $CollisionShape2D
onready var visibility_notifier = $VisibilityNotifier2D
onready var audio_player = $AudioStreamPlayer
onready var global = get_node("/root/Global")

export(int) var amount = 2
var is_temporary = false

func _ready():
	connect("area_entered", self, "_on_area_entered")
	visibility_notifier.connect("screen_exited", self, "_on_screen_exited")
	pass

func _on_area_entered(other):
	if other.TYPE == "player" and not ammo_label.is_full():
		audio_player.play()
		ammo_label.add_ammo(amount)
		collision_shape.disabled = true
		hide()
		increment_achievement_who_wouldnt_like_an_ammo_pack()
		
func respawn():
	if is_temporary:
		hide()
		collision_shape.disabled = true
		queue_free()
	else:
		collision_shape.disabled = false
		show()
		
func _on_screen_exited():
	if is_temporary:
		hide()
		collision_shape.disabled = true
		queue_free()
		
func increment_achievement_who_wouldnt_like_an_ammo_pack():
	var achievement = global.achievements_dictionary["WhoWouldntLikeAnAmmoPack"]
	if not achievement["unlocked"]:
		achievement["progress"] += 1
		if achievement["progress"] == achievement["goal"]:
			achievement["unlocked"] = true
			global.emit_unlocked_achievement("WhoWouldntLikeAnAmmoPack")
			global.save_achievements()