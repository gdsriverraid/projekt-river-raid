extends Control

onready var global = get_node("/root/Global")

func _ready():
	global.score = null
	global.leaderboard_new_position = null
	global.play_menu_soundtrack(true)
