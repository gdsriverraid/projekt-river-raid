extends Node2D


onready var global = get_node("/root/Global")
onready var tile = get_node("TileMap")
onready var tile2 = get_node("TileMap2")
const MapHeight = 2200;
export(int, 0, 10000) var scroll_speed = 300;

func _ready():
	global.set_stats_zeros()
	global.leaderboard_new_position = null

func _process(delta):

	var replacement = scroll_speed * delta;
	tile.position.y += replacement;
	tile2.position.y += replacement;

	if tile.position.y > MapHeight:
		tile.position.y = tile2.position.y - MapHeight;
	if tile2.position.y > MapHeight:
		tile2.position.y = tile.position.y - MapHeight;
