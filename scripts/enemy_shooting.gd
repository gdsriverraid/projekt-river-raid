extends "res://scripts/enemy.gd"

const SCORE = 120

onready var shooting_position = $ShootingPosition
onready var cooldown_timer = $CooldownTimer
onready var visibility_notifier = $VisibilityNotifier2D
var enemy_bullet_scene = preload("res://scenes/enemy_bullet.tscn")
onready var main_map_node = get_tree().get_root().get_node("Node/Map")
onready var player = get_tree().get_root().get_node("Node/Map/Player")
onready var player_bullets = get_tree().get_root().get_node("Node/Map/PlayerBullets")
onready var tile_map = get_parent().get_parent()
onready var collision_shape = $CollisionShape2D
onready var audio_shot = $AudioShot
onready var reaction_timer = $ReactionTimer
onready var audio_engine = $AudioEngine
onready var level_label = get_tree().get_root().get_node("Node/HUD/Level")

var explosion_scene = preload("res://scenes/helicopter_explosion.tscn")

enum State { INACTIVE, HUNT, SHOOT, ESCAPE }
var state = State.INACTIVE

var ready_to_shoot = true
export var speed = 300

enum Direction { LEFT, RIGHT }
var cant_go_further = false
var cant_go_further_direction = null
var on_body = false

var is_on_screen = false
var init_position = null

var reaction = false

func _ready():
	init_position = position
	randomize()
	visibility_notifier.connect("screen_entered", self, "_on_screen_entered")
	visibility_notifier.connect("screen_exited", self, "_on_screen_exited")
	cooldown_timer.connect("timeout", self, "_on_cooldown_timer_timeout")
	reaction_timer.connect("timeout", self, "_on_reaction_timer_timeout")
	connect("body_entered", self, "_on_body_entered")
	connect("body_exited", self, "_on_body_exited")
	get_node("Propeller/AnimationPlayer").play("propeller")

func _process(delta):
	delta = level_label.modify_delta_time(delta)
	if player.is_destroyed:
		state = State.INACTIVE
	
	match state:
		State.HUNT:
			hunt_state(delta)
		State.SHOOT:
			shoot_state()
		State.ESCAPE:
			escape_state(delta)
			
func _on_area_entered(other):
	base_on_area_entered(other, SCORE)
	if other.TYPE == "bullet":
		var explosion_instance = explosion_scene.instance()
		explosion_instance.position = position
		get_parent().add_child(explosion_instance)
		state = State.INACTIVE
		audio_engine.stop()
	
func _on_body_entered(other):
	on_body = true
	if state == State.ESCAPE:
		speed *= (-1)
		#$Sprite.flip_h = not $Sprite.flip_h
	elif state == State.HUNT:
		cant_go_further = true
		
func _on_body_exited(other):
	cant_go_further = false
	on_body = false
	print("enemy_shooting _on_body_exited")

func shoot():
	if ready_to_shoot:
		ready_to_shoot = false
		cooldown_timer.start()
		audio_shot.play()
		
		var enemy_bullet_instance = enemy_bullet_scene.instance()
		enemy_bullet_instance.position = get_parent().get_parent().position + position + shooting_position.position
		main_map_node.add_child(enemy_bullet_instance)
	
func _on_screen_entered():
	state = State.HUNT
	is_on_screen = true
	audio_engine.play()
	
func _on_screen_exited():
	state = State.INACTIVE
	is_on_screen = false
	
func _on_cooldown_timer_timeout():
	ready_to_shoot = true
	
func _on_reaction_timer_timeout():
	reaction = true
	
func hunt_state(delta):
	if is_in_player_position_range():
		state = State.SHOOT
	elif is_on_way_of_player_bullet():
		state = State.ESCAPE
		random_direction()
	else:
		go_towards_player(delta)
		
func is_in_player_position_range():
	var player_x = player.position.x
	var player_position_range = { 
		"left": player_x - 26,
		"right": player_x + 26
	}
	var shooting_position_x = position.x + shooting_position.position.x
	
	return shooting_position_x > player_position_range.left \
		and shooting_position_x < player_position_range.right
		
func go_towards_player(delta):
	if player.position.x > position.x \
			and ((cant_go_further and cant_go_further_direction != Direction.RIGHT) \
			or not cant_go_further):
				
		var new_position = position
		new_position.x += abs(speed * 0.7) * delta
		if is_new_position_safe(new_position):
			position = new_position
			if not on_body:
				cant_go_further_direction = Direction.RIGHT
			#if not $Sprite.flip_h:
				#$Sprite.flip_h = true
				
	elif player.position.x < position.x \
			and (cant_go_further and cant_go_further_direction != Direction.LEFT) \
			or not cant_go_further:
				
		var new_position = position
		new_position.x -= abs(speed) * delta
		if is_new_position_safe(new_position):
			position = new_position
			if not on_body:
				cant_go_further_direction = Direction.LEFT
			#if $Sprite.flip_h:
				#$Sprite.flip_h = false
			
func is_new_position_safe(new_position):
	for player_bullet in player_bullets.get_children():
		if player_bullet.position.x + 30 > new_position.x - 64 \
				and player_bullet.position.x - 30 < new_position.x + 64 \
				and player_bullet.position.y > tile_map.position.y + new_position.y - 64:
			return false
	return true
	
func shoot_state():
	shoot()
	if is_on_way_of_player_bullet():
		state = State.ESCAPE
		reaction = false
		randomize()
		reaction_timer.wait_time = rand_range(0.15, 0.4)
		reaction_timer.start()
		
		if on_body:
			set_direction()
		else:
			random_direction()
	elif not is_in_player_position_range():
		state = State.HUNT
	
func is_on_way_of_player_bullet():
	for player_bullet in player_bullets.get_children():
		if player_bullet.position.x + 30 > position.x - 64 \
				and player_bullet.position.x - 30 < position.x + 64 \
				and player_bullet.position.y > tile_map.position.y + position.y - 64:
			return true
	return false
	
func random_direction():
	var direction = randi() % 2
	if direction == 0:
		speed = abs(speed)
		#$Sprite.flip_h = true
	else:
		speed = -1 * abs(speed)
		#$Sprite.flip_h = false
		
func set_direction():
	if cant_go_further_direction == Direction.LEFT:
		speed = abs(speed)
		#$Sprite.flip_h = true
	else:
		speed = -1 * abs(speed)
		#$Sprite.flip_h = false
	
func escape_state(delta):
	if not is_on_way_of_player_bullet():
		state = State.HUNT
	else:
		if speed >= 0 and not on_body:
			cant_go_further_direction = Direction.RIGHT
		elif not on_body:
			cant_go_further_direction = Direction.LEFT
		if reaction:
			position.x += speed * delta
		
func respawn():
	position = init_position
	show()
	collision_poligon.disabled = false
	reaction = true
	cant_go_further = false
	cant_go_further_direction = null
	on_body = false
	if is_on_screen:
		state = State.HUNT
	else:
		state = State.INACTIVE
	
	for enemy_bullet in get_tree().get_nodes_in_group("enemy_bullet_group"):
		enemy_bullet.queue_free()
