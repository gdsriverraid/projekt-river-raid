extends Button

var event_scancode = null
export var key = ""

func _ready():
	connect("focus_entered", self, "_on_focus_entered")
	connect("focus_exited", self, "_on_focus_exited")

func _input(event):
	if event is InputEventKey and event.pressed and not event.echo and pressed:
		set_event_scancode(event.scancode)
		if event.scancode != KEY_ENTER and event.scancode != KEY_SPACE:
			pressed = false
		if event.scancode in [KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT, KEY_TAB]:
			accept_event()
			
func set_event_scancode(p_event_scancode):
	get_parent().remove_duplicates(p_event_scancode)
	event_scancode = p_event_scancode
	if p_event_scancode == null:
		set_text("")
	else:
		set_text(OS.get_scancode_string(p_event_scancode))
		
func _on_focus_entered():
	get_child(0).add_color_override("font_color", Color(1, 0, 0, 1))
	
func _on_focus_exited():
	get_child(0).add_color_override("font_color", Color(1, 1, 1, 1))