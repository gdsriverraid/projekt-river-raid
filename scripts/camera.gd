extends Camera2D

onready var player = get_node("../Player");
onready var shake_timer = $ShakeTimer
onready var level_label = get_tree().get_root().get_node("Node/HUD/Level")

var shake_offset = Vector2(0.0, 0.0)
var is_shake = false

func _ready():
	shake_timer.connect("timeout", self, "_on_shaker_timer_timeout")

func _process(delta):
	delta = level_label.modify_delta_time(delta)
	position.y -= player.speed * delta
	process_shake()
	
func process_shake():
	if is_shake:
		position -= shake_offset
		shake_offset = Vector2(rand_range(-10, 10), rand_range(-10, 10))
		position += shake_offset
	
func start_shake():
	is_shake = true
	shake_timer.start()
	
func _on_shaker_timer_timeout():
	is_shake = false