extends ScrollContainer

const SCROLL_SPEED = 1300

onready var global = get_node("/root/Global")
onready var items = get_node("VBoxContainer").get_children()

var achievements_without_progress = ["ThatsWhatImTalkingAbout", \
		"WeGotABadassOverHere", "OldSchool", "BullsEye", "AreYouImmortal"] 

func _ready():
	for item in items:
		var achievement = global.achievements_dictionary[item.get_name()]
		item.set_name(achievement["name"])
		item.set_icon(achievement["icon"])
		item.set_description(achievement["description"])
		item.set_difficulty(achievement["difficulty"])
		if item.get_name() in achievements_without_progress:
			item.set_progress(null)
		else:
			item.set_progress(achievement["progress"])
		item.set_unlocked(achievement["unlocked"])

func _process(delta):
	if Input.is_key_pressed(KEY_DOWN):
		scroll_vertical += SCROLL_SPEED * delta
	elif Input.is_key_pressed(KEY_UP):
		scroll_vertical -= SCROLL_SPEED * delta
		