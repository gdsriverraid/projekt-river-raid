extends Sprite

var max_fuel_amount = 100
var fuel_amount = 100
onready var global = get_node("/root/Global")
onready var audio_player = $AudioStreamPlayer
onready var player = get_tree().get_root().get_node("Node/Map/Player")
onready var timer = get_node("AudioStreamPlayer/Timer")

onready var hand = $Hand
onready var animation_player = get_node("LampEnabled/AnimationPlayer")
onready var lamp_enabled = $LampEnabled

var ready_to_play = true
var last_barrel = null
var achievement_last_barrel = null

func _ready():
	timer.connect("timeout", self, "_on_timeout")
	lamp_enabled.hide()
	
func _process(delta):
	if fuel_amount < 25:
		play_alarm()
		
func _on_timeout():
	ready_to_play = true

func decrease(amount):
	if fuel_amount < amount:
		amount = fuel_amount
	global.fuel_consumed += amount
	fuel_amount -= amount
	hand.rotation = (fuel_amount / max_fuel_amount) * PI
	#set_text("Fuel: %3.0f/%s" % [fuel_amount, max_fuel_amount])

func increase(amount):
	fuel_amount += amount
	if fuel_amount > max_fuel_amount:
		fuel_amount = max_fuel_amount
	if fuel_amount == max_fuel_amount:
		increment_achievement_fill_it_up()
	hand.rotation = (fuel_amount / max_fuel_amount) * PI
	#set_text("Fuel: %3.0f/%s" % [fuel_amount, max_fuel_amount])
	
func reset():
	lamp_enabled.hide()
	fuel_amount = max_fuel_amount
	last_barrel = null
	achievement_last_barrel = null
	hand.rotation = 0
	
func play_alarm():
	if ready_to_play and not player.is_destroyed and not audio_player.playing:
		lamp_enabled.show()
		ready_to_play = false
		audio_player.play()
		animation_player.play("FadeOut")
		timer.start()
		
func increment_achievement_fill_it_up():
	var achievement = global.achievements_dictionary["FillItUp"]
	if not achievement.unlocked and achievement_last_barrel != last_barrel:
		achievement_last_barrel = last_barrel
		achievement.progress += 1
		if achievement.progress == achievement.goal:
			achievement.unlocked = true
			global.save_achievements()
			global.emit_unlocked_achievement("FillItUp")