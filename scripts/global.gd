extends Node

signal unlocked_achievement(achievement_id)

var score = null
var defeated_enemies = null
var distance = null
var fuel_consumed = null
var collected_powerups = null
var number_of_shots = null
var number_of_accurate_shots = null
var longest_kill_strike = null

var leaderboard_new_position = null

var menu_soundtrack = preload("res://scenes/menu_soundtrack.tscn")
var menu_soundtrack_instance

var sound_button = preload("res://scenes/sound_button.tscn")
var sound_button_instance

# settings
var settings_dictionary = null
const SETTINGS_FILE_PATH = "user://settings.json"
const ACHIEVEMENTS_FILE_PATH = "user://achievements.json"
var master_bus_volume_db

var master_bus_idx = AudioServer.get_bus_index("Master")

# achievements
var achievements_dictionary = null

func load_settings():
	var settings_file = File.new()
	if settings_file.file_exists(SETTINGS_FILE_PATH):
		settings_file.open(SETTINGS_FILE_PATH, File.READ)
		settings_dictionary = parse_json(settings_file.get_as_text())
		settings_file.close()
	else:
		settings_dictionary = create_settings_dictionary()
				
func create_settings_dictionary():
	return { "player_1_down":16777234,
			"player_1_fire":32,
			"player_1_left":16777231,
			"player_1_right":16777233,
			"player_1_up":16777232,
			"sound_volume":50}

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	load_settings()
	load_achievements()
	menu_soundtrack_instance = menu_soundtrack.instance()
	sound_button_instance = sound_button.instance()
	add_child(menu_soundtrack_instance)
	add_child(sound_button_instance)
	master_bus_volume_db = AudioServer.get_bus_volume_db(master_bus_idx)
	change_volume(settings_dictionary["sound_volume"])

func change_volume(value):
	if value > 0:
		AudioServer.set_bus_volume_db(master_bus_idx, (value / 2.0) - 35)
		AudioServer.set_bus_mute(master_bus_idx, false)
	else:
		AudioServer.set_bus_mute(master_bus_idx, true)
	
func play_menu_soundtrack(play):
	if play and not menu_soundtrack_instance.playing:
		menu_soundtrack_instance.play()
	elif not play:
		menu_soundtrack_instance.stop()

func set_stats_zeros():
	score = 0
	defeated_enemies = 0
	distance = 0
	fuel_consumed = 0
	collected_powerups = 0
	number_of_shots = 0
	number_of_accurate_shots = 0
	longest_kill_strike = 0
	
func play_button_sound():
	sound_button_instance.play()
	
# achievements
func load_achievements():
	var achievements_file = File.new()
	if achievements_file.file_exists(ACHIEVEMENTS_FILE_PATH):
		achievements_file.open(ACHIEVEMENTS_FILE_PATH, File.READ)
		achievements_dictionary = parse_json(achievements_file.get_as_text())
		achievements_file.close()
	else:
		achievements_dictionary = create_achievements_dictionary()
		
func create_achievements_dictionary():
	return {
			"HeliAbove": {
				"name": "Heli above!",
				"icon": "heli_above.png",
				"difficulty": "Easy",
				"description": "Destroy 10 helicopters",
				"progress": 0,
				"goal": 10,
				"unlocked": false
			},
			"IdRatherFly": {
				"name": "I'd rather fly",
				"icon": "id_rather_fly.png",
				"difficulty": "Easy",
				"description": "Destroy 10 tankers",
				"progress": 0,
				"goal": 10,
				"unlocked": false
			},
			"NotThisTime": {
				"name": "Not this time",
				"icon": "not_this_time.png",
				"difficulty": "Easy",
				"description": "Destroy 10 kamikazes",
				"progress": 0,
				"goal": 10,
				"unlocked": false
			},
			"WhosNext": {
				"name": "Who's next?",
				"icon": "whos_next.png",
				"difficulty": "Easy",
				"description": "Destroy 10 planes",
				"progress": 0,
				"goal": 10,
				"unlocked": false
			},
			"WhoWouldntLikeAnAmmoPack": {
				"name": "Who wouldn't like an ammo pack?",
				"icon": "who_wouldnt_like_an_ammo_pack.png",
				"difficulty": "Easy",
				"description": "Collect 10 ammo packages",
				"progress": 0,
				"goal": 10,
				"unlocked": false
			},
			"Souvenirs": {
				"name": "Souvenirs!",
				"icon": "souvenirs.png",
				"difficulty": "Easy",
				"description": "Collect 2 lifes",
				"progress": 0,
				"goal": 2,
				"unlocked": false
			},
			"SlowMo": {
				"name": "Slow mo",
				"icon": "slow_mo.png",
				"difficulty": "Medium",
				"description": "Collect 25 slowing powerups",
				"progress": 0,
				"goal": 25,
				"unlocked": false
			},
			"YouGotTheMoves": {
				"name": "You got the moves!",
				"icon": "you_got_the_moves.png",
				"difficulty": "Medium",
				"description": "Collect 25 faster shooting powerups",
				"progress": 0,
				"goal": 25,
				"unlocked": false
			},
			"FillItUp": {
				"name": "Fill it up",
				"icon": "fill_it_up.png",
				"difficulty": "Medium",
				"description": "Fill up 25 times",
				"progress": 0,
				"goal": 25,
				"unlocked": false
			},
			"DontStopMe": {
				"name": "Don't stop me",
				"icon": "dont_stop_me.png",
				"difficulty": "Medium",
				"description": "Get 5,000 points",
				"progress": 0,
				"goal": 5000,
				"unlocked": false
			},
			"ThatsWhatImTalkingAbout": {
				"name": "That's what I'm talking about",
				"icon": "thats_what_im_talking_about.png",
				"difficulty": "Medium",
				"description": "Collect 2 powerups each type in the same game",
				"progress_slowing": 0,
				"progress_faster": 0,
				"progress_lifes": 0,
				"goal_slowing": 2,
				"goal_faster": 2,
				"goal_lifes": 2,
				"unlocked": false
			},
			"GoingForIt": {
				"name": "Going for it",
				"icon": "going_for_it.png",
				"difficulty": "Hard",
				"description": "Get 25,000 points",
				"progress": 0,
				"goal": 25000,
				"unlocked": false
			},
			"GimmeMore": {
				"name": "Gimme more!",
				"icon": "gimme_more.png",
				"difficulty": "Hard",
				"description": "Destroy 250 enemies",
				"progress": 0,
				"goal": 250,
				"unlocked": false
			},
			"WeGotABadassOverHere": {
				"name": "We got a badass over here!",
				"icon": "we_got_a_badass_over_here.png",
				"difficulty": "Hard",
				"description": "Go through the map without losing your life",
				"progress": 0,
				"unlocked": false
			},
			"Suicadal": {
				"name": "Suicadal",
				"icon": "suicadal.png",
				"difficulty": "Hard",
				"description": "Destroy 30 kamikazes",
				"progress": 0,
				"goal": 150,
				"unlocked": false
			},
			"ThatWouldBeIt": {
				"name": "That would be it",
				"icon": "that_would_be_it.png",
				"difficulty": "Very hard",
				"description": "Destroy 500 enemies",
				"progress": 0,
				"goal": 500,
				"unlocked": false
			},
			"WowNiceOne": {
				"name": "Wow, nice one!",
				"icon": "wow_nice_one.png",
				"difficulty": "Very hard",
				"description": "Get 50,000 points",
				"progress": 0,
				"goal": 50000,
				"unlocked": false
			},
			"OldSchool": {
				"name": "Old school",
				"icon": "old_school.png",
				"difficulty": "Very hard",
				"description": "Go through the map without using powerups",
				"progress": 0,
				"unlocked": false
			},
			"BullsEye": {
				"name": "Bulls eye!",
				"icon": "bulls_eye.png",
				"difficulty": "Very hard",
				"description": "Do not miss the whole level",
				"progress": 0,
				"unlocked": false
			},
			"AreYouImmortal": {
				"name": "Are you immortal?",
				"icon": "are_you_immortal.png",
				"difficulty": "Very hard",
				"description": "Go through 5 maps without losing a life",
				"progress": 0,
				"unlocked": false
			}
	}
	
func save_achievements():
	var achievements_file = File.new()
	achievements_file.open(ACHIEVEMENTS_FILE_PATH, File.WRITE)
	achievements_file.store_string(to_json(achievements_dictionary))
	achievements_file.close()
	
func unlock_achievement(achievement_id):
	var achievement = achievements_dictionary[achievement_id]
	if not achievement["unlocked"]:
		achievement["unlocked"] = true
		return true
		
func reset_achievement_thats_what_im_talking_about():
	var achievement = achievements_dictionary["ThatsWhatImTalkingAbout"]
	if not achievement["unlocked"]:
		achievement["progress_slowing"] = 0
		achievement["progress_faster"] = 0
		achievement["progress_lifes"] = 0
		
func reset_achievement_bulls_eye():
	var achievement = achievements_dictionary["BullsEye"]
	if not achievement["unlocked"]:
		achievement["progress"] = 0
		
func reset_achievement_old_school():
	var achievement = achievements_dictionary["OldSchool"]
	if not achievement["unlocked"]:
		achievement["progress"] = 0
		
func reset_achievement_are_you_immortal():
	var achievement = achievements_dictionary["AreYouImmortal"]
	if not achievement["unlocked"]:
		achievement["progress"] = 0
		
func emit_unlocked_achievement(achievement_id):
	print("emit_unlocked_achievement %s" % achievement_id)
	emit_signal("unlocked_achievement", achievement_id)
